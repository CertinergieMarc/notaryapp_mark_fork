import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule, routingComponents } from './app.routing.component';

@NgModule({
  imports:      [ BrowserModule, AppRoutingModule, ModalModule ],
  declarations: [ AppComponent, routingComponents ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
