﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotaryComponent } from './Notary/notary.first.component';

const routes: Routes = [
    {
        path: 'firstcomponent',
        component: NotaryComponent
    }
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }

export const routingComponents = [NotaryComponent]
